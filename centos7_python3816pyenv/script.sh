#!/bin/bash
yum update -y
yum install zlib-devel bzip2-devel openssl-devel ncurses-devel sqlite-devel readline-devel tk-devel gdbm-devel db4-devel libpcap-devel xz-devel git gcc make libffi-devel -y
# yum install zlib-devel bzip2-devel sqlite sqlite-devel openssl-devel -y
mkdir /pyenv
git clone https://github.com/pyenv/pyenv.git /pyenv  
echo 'export PYENV_ROOT="/pyenv"'>> /root/.bashrc
echo 'export PATH="$PYENV_ROOT/bin:$PATH"'>>/root/.bashrc  
echo 'eval "$(pyenv init -)"'>>/root/.bashrc  
exec $SHELL -l 
pyenv install 3.8
pyenv global 3.8
pip install --upgrade pip
yum clean all